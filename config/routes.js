/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {


  //  ╦ ╦╔═╗╔╗ ╔═╗╔═╗╔═╗╔═╗╔═╗
  //  ║║║║╣ ╠╩╗╠═╝╠═╣║ ╦║╣ ╚═╗
  //  ╚╩╝╚═╝╚═╝╩  ╩ ╩╚═╝╚═╝╚═╝

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  '/': {
    view: 'pages/homepage',
    locals: {
      layout: 'layouts/basic'
    }
  },

  '/alumno':{
    view: 'pages/alumnos/inicio',
    locals:{
      layout: 'layouts/alumno'
    }
  },

  '/alumno/solicitar':{
    view: 'pages/alumnos/solicitar',
    locals:{
      layout: 'layouts/alumno'
    }
  },

  '/alumno/tramite':{
    view: 'pages/alumnos/tramite',
    locals:{
      layout: 'layouts/alumno'
    }
  },

  '/jefe':{
    view: 'pages/jefes/inicio',
    locals:{
      layout: 'layouts/jefe'
    }
  },

  '/jefe/tramite':{
    view: 'pages/jefes/tramite',
    locals:{
      layout: 'layouts/jefe'
    }
  },

  '/division':{
    view: 'pages/division/inicio',
    locals:{
      layout: 'layouts/division'
    }
  },

  '/division/tramite':{
    view: 'pages/division/tramite',
    locals:{
      layout: 'layouts/division'
    }
  },

  '/departamento':{
    view: 'pages/departamento/inicio',
    locals:{
      layout: 'layouts/departamento'
    }
  },

  '/departamento/tramite':{
    view: 'pages/departamento/tramite',
    locals:{
      layout: 'layouts/departamento'
    }
  },

  '/departamento/tramite/salones':{
    view: 'pages/departamento/salones',
    locals:{
      layout: 'layouts/departamento'
    }
  },


  '/departamento/tramite/salon/agendar':{
    view: 'pages/departamento/calendario',
    locals:{
      layout: false
    }
  }

  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


  //  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
  //  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
  //  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝



  //  ╦ ╦╔═╗╔╗ ╦ ╦╔═╗╔═╗╦╔═╔═╗
  //  ║║║║╣ ╠╩╗╠═╣║ ║║ ║╠╩╗╚═╗
  //  ╚╩╝╚═╝╚═╝╩ ╩╚═╝╚═╝╩ ╩╚═╝


  //  ╔╦╗╦╔═╗╔═╗
  //  ║║║║╚═╗║
  //  ╩ ╩╩╚═╝╚═╝


};
